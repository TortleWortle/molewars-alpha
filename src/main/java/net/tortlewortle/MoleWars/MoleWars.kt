package net.tortlewortle.MoleWars

import net.tortlewortle.MoleWars.Game.MoleWarsGame
import org.bukkit.Bukkit
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerBedLeaveEvent
import org.bukkit.plugin.java.JavaPlugin


/**
 * Created by TortleWortle on 08/12/2016.
 */

class MoleWars : JavaPlugin(), Listener, CommandExecutor {
    var game : MoleWarsGame? = null
    init {
        HOLDER.INSTANCE = this
    }
    override fun onEnable() {
        config.options().copyDefaults(true)
        saveResource("schematics/Main.schematic", false);
        saveConfig()
        game = MoleWarsGame("Main")

        server.pluginManager.registerEvents(this, this)
        server.pluginManager.registerEvents(game, this)


        Bukkit.unloadWorld("world_nether", false)
        Bukkit.unloadWorld("world_the_end", false)

        getCommand("startGameLel").executor = this

    }
    override fun onDisable() {
        game?.endGame()
    }
    private object HOLDER {
        var INSTANCE : MoleWars? = null
    }
    companion object {
        // Should only be accessed after Bukkit inits the ploogin
        fun getInstance() : MoleWars {
            return HOLDER.INSTANCE!!
        }
    }

    //temporary for some reason in here
    @EventHandler fun onDing(event: PlayerBedLeaveEvent) {
        event.player.teleport(game!!.world.spawnLocation)
    }

    override fun onCommand(sender: CommandSender?, command: Command?, label: String?, args: Array<out String>?): Boolean {
        game!!.startGame()
        return true
    }
}