package net.tortlewortle.MoleWars.Game

import com.google.common.hash.Hashing
import com.sk89q.worldedit.CuboidClipboard
import com.sk89q.worldedit.blocks.BaseBlock
import com.sk89q.worldedit.blocks.BlockID
import com.sk89q.worldedit.blocks.BlockType
import com.sk89q.worldedit.bukkit.BukkitWorld
import com.sk89q.worldedit.bukkit.WorldEditPlugin
import net.tortlewortle.MoleWars.MoleWars
import org.bukkit.*
import org.bukkit.block.Chest
import org.bukkit.enchantments.Enchantment
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.block.BlockBreakEvent
import org.bukkit.event.entity.EntityDamageEvent
import org.bukkit.event.entity.FoodLevelChangeEvent
import org.bukkit.event.player.*
import org.bukkit.inventory.ItemFlag
import org.bukkit.inventory.ItemStack
import org.bukkit.potion.PotionEffect
import org.bukkit.potion.PotionEffectType
import org.bukkit.util.Vector
import java.io.File
import java.util.*
import java.util.concurrent.ThreadLocalRandom
import com.sk89q.worldedit.Vector as weVector


/**
 * Created by TortleWortle on 08/12/2016.
 */
class MoleWarsGame (name: String) : Listener {
    var name = name
    var plugin = MoleWars.getInstance()
    var worldName = plugin.config.getString("worldPrefix") + name
    var world = createWorld()
    var spawnLocations = HashSet<Location>()
    var chestLocations = HashSet<Location>()
    var availableSpawns = HashSet<Location>()
    var linkedSpawns = HashMap<Player, Location>()
    var started = false
    var ended = false

    var currentPlayers = HashSet<Player>()
    var alivePlayers = HashSet<Player>()

    var we = plugin.server.pluginManager.getPlugin("WorldEdit") as WorldEditPlugin

    var timeLeft = 0

    var weCenterLoc = weVector(0, 40, 0)
    var centerLoc = Vector(0, 40, 0)
    var spSpawnLoc = Location(world, 0.0, 80.0, 0.0)


    init {
        prepareWorld()
        Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, {
            tick()
        }, 20L, 1L)
    }
    //gets called every 1 tick, 20 tick = 1 second
    private fun tick() {
        //if game hasn't started and can start, start!
        if(!started && canStart()) {
            //start the game
            startGame()
        }
        if(alivePlayers.count() == 1 && !ended) {
            winGame(alivePlayers.first())
        }
    }
    fun unLoad() {

    }

    private fun winGame(p: Player) {
        ended = true
        p.sendTitle("You won!", "Also you're gay.")
        Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, {
            Bukkit.getServer().reload()
        }, 20 * 5)
    }

    private fun prepareWorld() {
        //Paste schematic
        //For now place block or something
        var schematic = File("plugins/MoleWars/schematics/" + name + ".schematic")
        var session = we.worldEdit.editSessionFactory.getEditSession(BukkitWorld(world), 2000000)
        var cc = CuboidClipboard.loadSchematic(schematic)
        cc.offset = weVector(0, 0, 0)

//        weCenterLoc.add(cc.origin.multiply(-1))
        System.out.println(cc.origin.toString())

                            println(cc.width)
                            println(cc.height)
                            println(cc.length)
                            for(x in 0..cc.width-1){
                                for (y in 0..cc.height-1) {
                                    for (z in 0..cc.length-1) {
                                        var currentVector = weVector(x, y, z)
                                        var currentBlock = cc.getBlock(currentVector)

                                        //check for glass with air spaces above and add to spawnlist
                                        if(currentBlock.type == BlockType.GLASS.id) {
                                            if(cc.getBlock(weVector(x, y+1, z)).type == BlockType.AIR.id
                                                    && cc.getBlock(weVector(x, y+2, z)).type == BlockType.AIR.id) {
                                                //is spawn block
                                                System.out.println("Found spawnpoints:")
                                                System.out.println("X: " + x)
                                                System.out.println("Y: " + y)
                                                System.out.println("Z: " + z)
                                                var spawnLoc = Location(world, x.toDouble(), y.toDouble(), z.toDouble())
                                                spawnLoc.add(Vector(0.5, 1.0, 0.5))
                                                spawnLoc.add(centerLoc)
                            spawnLoc.add(convertBVec(cc.offset))
                            spawnLocations.add(spawnLoc)
                        }
                    }
                    if(currentBlock.type == BlockType.CHEST.id || currentBlock.type == BlockType.TRAPPED_CHEST.id) {
                        var chestLoc = Location(world, x.toDouble(), y.toDouble(), z.toDouble())
                        chestLoc.add(centerLoc)
                        chestLoc.add(convertBVec(cc.offset))
                    }
                }
            }
        }

        availableSpawns = spawnLocations

        cc.paste(session, weCenterLoc, false)
    }
    private fun convertBVec(wev: weVector) : Vector{
        return Vector(wev.blockX, wev.blockY, wev.blockZ)
    }
    private fun convertBVec(v: Vector) : weVector {
        return weVector(v.blockX, v.blockY, v.blockZ)
    }
    private fun createWorld(): World {
        if(Bukkit.getWorld(worldName) != null){
            deleteWorld(Bukkit.getWorld(worldName).worldFolder)
        }
        deletePlayerData(Bukkit.getWorlds().first())

        var world = WorldCreator(worldName)
        world.generateStructures(false)
        world.type(WorldType.FLAT)
        world.generatorSettings("2;0;1")
        var cworld = world.createWorld()
        cworld.animalSpawnLimit = 0
        cworld.monsterSpawnLimit = 0
        return cworld
    }
    fun join(p : Player) {
        if(started){
            p.kickPlayer("Game already started!")
        }
        var spawn = availableSpawns.firstOrNull()
        if(spawn != null) {
            p.teleport(spawn)
            linkedSpawns.put(p, spawn)
            availableSpawns.remove(spawn)
            p.gameMode = GameMode.SURVIVAL
            p.health = 20.0
            p.foodLevel = 20
            //prepare inventory for kits or something idk
            p.inventory.clear()
            currentPlayers.add(p)
        }else {
            p.kickPlayer("Game full!")
        }
    }
    fun leave(p : Player) {
        if(!started){
            currentPlayers.remove(p)
            var linked = linkedSpawns.get(p)
            if(linked != null) {
                availableSpawns.add(linked)
                linkedSpawns.remove(p)
            }
        }else {
            playerDied(p)
        }
    }
    private fun canStart() : Boolean{
        if(availableSpawns.count() == 0) {
            return true
        }
        return false
    }
    fun startGame() {
        started = true
        //Do something start timer whatever.

        //fill chests
        chestLocations.forEach { location ->
            var block = world.getBlockAt(location)
            if(block.type == Material.CHEST || block.type == Material.TRAPPED_CHEST) {
                var chest = block as Chest
                var r = ThreadLocalRandom.current().nextInt(1, 64)
                chest.inventory.addItem(ItemStack(Material.GOLDEN_APPLE, r))
            }
        }

        //set alivePlayers to currentPlayers.
        alivePlayers = currentPlayers

        //prepare player inventory
        alivePlayers.forEach { player ->
            player.inventory.clear()
            var shovel = ItemStack(Material.DIAMOND_SPADE)
            var shovelM = shovel.itemMeta
            shovelM.addEnchant(Enchantment.DURABILITY, 1000, true)
            shovelM.addEnchant(Enchantment.DIG_SPEED, 2, false)
            shovelM.displayName = "Mole Claw"
            shovelM.itemFlags.add(ItemFlag.HIDE_ENCHANTS)
            shovel.itemMeta = shovelM
            player.inventory.addItem(shovel)
            player.inventory.addItem(ItemStack(Material.GOLDEN_APPLE, 10))

            player.inventory.addItem(ItemStack(Material.DIAMOND_SWORD))
            player.inventory.addItem(ItemStack(Material.IRON_BOOTS))
            player.inventory.addItem(ItemStack(Material.IRON_LEGGINGS))
            player.inventory.addItem(ItemStack(Material.IRON_HELMET))
            player.inventory.addItem(ItemStack(Material.IRON_CHESTPLATE))

            player.addPotionEffect(PotionEffect(PotionEffectType.NIGHT_VISION, 9999999, 1, false,false))

        }
    }
    fun endGame() {
        if(!world.players.isEmpty()){
            world.players.forEach { player -> player.kickPlayer("Game resetting please wait!") }
        }
        Bukkit.unloadWorld(world, false)
        deleteWorld(world.worldFolder)
    }
    private fun deletePlayerData(w: World) {
        deleteWorld(File(w.worldFolder, "playerdata"))
        deleteWorld(File(w.worldFolder, "stats"))
    }
    private fun deleteWorld(path: File): Boolean {
        if (path.exists()) {
            val files = path.listFiles()
            for (i in files!!.indices) {
                if (files[i].isDirectory) {
                    deleteWorld(files[i])
                } else {
                    files[i].delete()
                }
            }
        }
        return path.delete()
    }
    fun resetWorld() {
        endGame()
        world = createWorld()
        prepareWorld()
    }

    fun playerDied(p: Player) {
        alivePlayers.remove(p)
        p.inventory.contents.forEach { ItemStack -> if(ItemStack != null) world.dropItemNaturally(p.location, ItemStack) }
        p.inventory.armorContents.forEach { ItemStack -> if(ItemStack != null) world.dropItemNaturally(p.location, ItemStack) }
        p.inventory.clear()
        moveToSpec(p)
    }
    fun moveToSpec(p: Player) {
        p.gameMode = GameMode.SPECTATOR
        p.teleport(spSpawnLoc)
    }
    @EventHandler fun onJoin(event: PlayerJoinEvent) {
        join(event.player)
    }
    @EventHandler fun onQuit(event: PlayerQuitEvent) {
        leave(event.player)
    }

    @EventHandler fun onDamage(event: EntityDamageEvent) {
        if(event.entity is Player) {
            var p = event.entity as Player
            if((p.health - event.finalDamage <= 0) || (event.cause.equals(EntityDamageEvent.DamageCause.VOID))){
                playerDied(p)
                event.isCancelled = true
            }
        }
    }
    @EventHandler fun onJoinAttempt(event: PlayerLoginEvent) {
        if(started) {
            event.disallow(PlayerLoginEvent.Result.KICK_OTHER, "Game already started!")
        }
    }

    @EventHandler fun onMove(e: PlayerMoveEvent) {
        if(!started){
            if(e.getFrom().getX() != e.getTo().getX() || e.getFrom().getZ() != e.getTo().getZ()) {
                e.isCancelled = true
            }
        }
    }
    @EventHandler fun onDrop(e: PlayerDropItemEvent) {
        if(!started) {
            e.isCancelled = true
        }
    }
    @EventHandler fun blockBreak(e: BlockBreakEvent) {
        if(!started) {
            e.isCancelled = true
        }
    }
    @EventHandler fun onFoodLose(event: FoodLevelChangeEvent) {
        if(!started) {
            event.isCancelled = true
        }
    }
}